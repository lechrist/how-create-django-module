# How-create-django-module
For this documentation I suppose you have already a django app reusable. For this documentation our django app will named **authapp**.
Python packaging refers to preparing your app in a specific format that can be easily installed and used. Django itself is packaged very much like this.

1. First, create a parent directory for **authapp**, outside of your Django project. Call this directory **django-authapp**.

        Choosing a name for your app

        When choosing a name for your package, check resources like PyPI to avoid naming conflicts with existing packages. 
        It’s often useful to prepend django- to your module name when creating a package to distribute. 
        This helps others looking for Django apps identify your app as Django specific.

        Application labels (that is, the final part of the dotted path to application packages) must be unique in INSTALLED_APPS. 
        Avoid using the same label as any of the Django contrib packages, for example auth, admin, or messages.

2. Move the authapp directory into the django-authapp directory.

3. Create a file django-authapp/README.rst with the following contents:
         
        ## django-authapp/README.rst

        =======
        Authapp
        =======

        Authapp is a simple Django app to to manage all authentication urls for your backend api.
        Detailed documentation is in the "docs" directory.

        Quick start
        -----------

        1. Add "authapp" to your INSTALLED_APPS setting like this::

            INSTALLED_APPS = [
                ...
                'authapp',
            ]

        2. Include the authapp URLconf in your project urls.py like this::

            path('authapp/', include('authapp.urls')),

        3. Run `**python manage.py migrate**` to create the authapp models.

        4. Visit http://127.0.0.1:8000/authapp/ to see all authentication urls.
        
4. Create a django-authapp/LICENSE file. Choosing a license is beyond the scope of this tutorial, but suffice it to say that code released publicly without a license is useless. Django and many Django-compatible apps are distributed under the BSD license; however, you’re free to pick your own license. Just be aware that your licensing choice will affect who is able to use your code.

5. Next we’ll create setup.cfg and setup.py files which detail how to build and install the app. A full explanation of these files is beyond the scope of this tutorial, but the setuptools documentation has a good explanation. Create the files django-authapp/setup.cfg and django-authapp/setup.py with the following contents:

        ## django-authapp/setup.cfg

        [metadata]
        name = django-authapp
        version = 0.1
        description = A Django app to manage all authentication urls for your backend api.
        long_description = file: README.rst
        url = https://www.example.com/
        author = Your Name
        author_email = yourname@example.com
        license = BSD-3-Clause  # Example license
        classifiers =
            Environment :: Web Environment
            Framework :: Django
            Framework :: Django :: X.Y  # Replace "X.Y" as appropriate
            Intended Audience :: Developers
            License :: OSI Approved :: BSD License
            Operating System :: OS Independent
            Programming Language :: Python
            Programming Language :: Python :: 3
            Programming Language :: Python :: 3 :: Only
            Programming Language :: Python :: 3.6
            Programming Language :: Python :: 3.7
            Programming Language :: Python :: 3.8
            Topic :: Internet :: WWW/HTTP
            Topic :: Internet :: WWW/HTTP :: Dynamic Content

        [options]
        include_package_data = true
        packages = find:

    ## django-authapp/setup.py

        from setuptools import setup

        setup()

6.  Only Python modules and packages are included in the package by default. To include additional files, we’ll need to create a MANIFEST.in file. The setuptools docs referred to in the previous step discuss this file in more details. To include the templates, the README.rst and our LICENSE file, create a file django-authapp/MANIFEST.in with the following contents:

        include LICENSE
        include README.rst
        recursive-include authapp/static *
        recursive-include authapp/templates *
        recursive-include docs *
   _ Note that the docs directory won’t be included in your package unless you add some files to it. Many Django apps also provide their documentation online through sites like [readthedocs.org](https://readthedocs.org/)._

7. Try building your package with **python setup.py sdist** (run from inside django-authapp). This creates a directory called dist and builds your new package, django-authapp-0.1.tar.gz.

## Using your own package locally

1. To install the package, use pip (you already installed it, right?):

        **pip install --user django-authapp/dist/django-authapp-0.1.tar.gz**
2. With luck, your Django project should now work correctly again. Run the server again to confirm this.

3. To uninstall the package, use pip:

        **pip uninstall django-authapp**
## Publishing your app

1. Register an account on https://pypi.org - note that these are two separate servers and the login details from the test server are not shared with the main server. So after you do that we will use twine to deploy our package or module

2. You’ll need to install Twine to upload the distribution packages
        **python3 or py -m pip install --user --upgrade twine**

3. To check is everything is correct run:
        **python3 or py -m twine check dist/***

4. To deploy run:
        **python3 or py -m twine upload dist/***

5. To update your package change his version django-authapp/setup.cfg and run the third and fourth items of Publishing section


# Sources
#### [https://docs.djangoproject.com/en/2.2/intro/reusable-apps/](https://docs.djangoproject.com/en/2.2/intro/reusable-apps/)
#### [https://packaging.python.org/tutorials/packaging-projects/](https://packaging.python.org/tutorials/packaging-projects/)
#### [https://pypi.org ](https://pypi.org )
#### [https://packaging.python.org/key_projects/#twine](https://packaging.python.org/key_projects/#twine)

> By Mardochée KIKIGBAGBAN 


